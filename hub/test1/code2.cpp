//c++ function
#include <iostream>
using namespace std;

int main(){
    double r,h;
    double PI = 3.14;
    cout << "请输入底面圆的半径和圆柱的高（在两个数据中间加上空格）：";
    cin >> r >> h;
    cout << "圆柱的底面圆周长为：" << 2*PI*r << endl;
    cout << "圆柱的底面圆面积为：" << PI*r*r << endl;
    cout << "圆柱的侧面积为：" << h*2*PI*r << endl;
    cout << "圆柱的全面积为：" << r*PI*r+2*PI*r*h << endl;
    cout << "圆柱的体积为：" << r*PI*r*h << endl;
    cout << "运算完毕\n" << endl;
    system("pause");
    return 0;
}
