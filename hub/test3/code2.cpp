#include <iostream>
using namespace std;

int main (){
    int year;
    cout << "请输入年份：";
    cin >> year;
    switch (year % 12)
    {
        case 0:
            cout << "猴年。\n";
            break;
        case 1:
            cout << "鸡年。\n";
            break;
        case 2:
            cout << "狗年。\n";
            break;
        case 3:
            cout << "猪年。\n";
            break;
        case 4:
            cout << "鼠年。\n";
            break;
        case 5:
            cout << "牛年。\n";
            break;
        case 6:
            cout << "虎年。\n";
            break;
        case 7:
            cout << "兔年。\n";
            break;
        case 8:
            cout << "龙年。\n";
            break;
        case 9:
            cout << "蛇年。\n";
            break;
        case 10:
            cout << "马年。\n";
            break;
        case 11:
            cout << "羊年。\n";
            break;
    }

    system("pause");
    return 0;    
}
